const defaults = require('../defaults');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const completeObj = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham',
    hobbies: ['movies', 'cricket']
}

const defaultsResult = defaults(testObject, completeObj);
console.log(defaultsResult);