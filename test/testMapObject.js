const mapObject = require('../mapObject');

const testObject = { number1: 3, number2: 5, number3: 7 };

function double(value, key) {
    return value * 2
}


console.log(`test object before mapping`)
console.log(testObject);


const mapResult = mapObject(testObject, double);


console.log(`test object after mapping`)
console.log(mapResult);

