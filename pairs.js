function pairs(obj) {
    const pairsArray = [];
    for(let key in obj){
        pairsArray.push(Array(key, obj[key]));
    }
    return pairsArray;
}

module.exports = pairs;