function mapObject(obj, callback){
    for(let key in obj){
        obj[key] = callback(obj[key], key)
    }
    return obj;
}

module.exports = mapObject;